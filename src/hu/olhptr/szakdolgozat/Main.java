package hu.olhptr.szakdolgozat;
import java.io.File;
import java.io.IOException;


/**
 * Hasznalat:          pontosan 2 argumentumot kell adni parancssorbol az alkalmazasnak.<p>
 * Elso argumentum:    a beolvasando szoveges dokumentum, lehetoleg *.txt kiterjesztessel.
 * Masodik argumentum: a mentesi hely ahova a metrikak, ... keruljenek.
 *                     Amennyiben nincs ilyen mappa, akkor automatikusan letrehozasra kerul. 
 * Pl.: "d:\projects\input.txt" "d:\projects\result"
 * @param args[0] input szoveges fajl, pl.: "d:\projects\input.txt"
 * @param args[1] kimeneti mappa, pl.: "d:\projects\result"
 * @author Olah Peter 
 * @email h060263@stud.u-szeged.hu
 * @email olhptr@gmail.com
 */
public class Main {
	public static void main(String[] args) {
		try
		{
			if (args.length < 2) {
				System.out.println("Keves argumentumot adtal meg!");
				return;
			} else if (args.length == 2){
				
				File inputTxt = new File(args[0]);
				if (!inputTxt.isFile()){
					System.out.println("Az elso parameterkent megadott ertek nem fajl!");
					return;
				}
				
				File destinationFolder = new File(args[1]);
				if (!destinationFolder.exists() || !destinationFolder.isDirectory()) destinationFolder.mkdir();

				ProjectHandler projectHandler = new ProjectHandler(inputTxt, destinationFolder);
				projectHandler.executeActions();

				System.out.println("Beolvasott input fajl:      "+inputTxt.getAbsolutePath());
				System.out.println("Mentesi mappa utvonala:     "+destinationFolder.getAbsolutePath());
				System.out.println("Beolvasott projektek szama: "+projectHandler.getNumberOfProjects());
				System.out.println("Ervenyes projektek szama:   "+projectHandler.getNumberOfValidProjects());

			} else if (args.length > 2) {
				System.out.println("Sok argumentum, pontosan 2 -t adj meg!");
				return;
			}
			
		} catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}
}