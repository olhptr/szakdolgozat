package hu.olhptr.szakdolgozat;

/**
 * Osztaly a projektek tarolasara.
 */
public class Project {
	
	private String name = null;
	private String folder = null;
	private String resFolder = null;
	private float rating;
	private int installations;

	/**
	 * Project konstruktora.
	 * @param name          a projekt neve
	 * @param folder        a projekt fomappaja
	 * @param rating        Google Play ertekelese
	 * @param installations telepitesek szama
	 */
	public Project(String name, String folder, float rating, int installations) {
		this.name = name;
		this.folder = folder;
		this.resFolder = folder + "/res";
		this.rating = rating;
		this.installations = installations;
	}
	
	/**
	 * Visszaadja a projekt alapertelmezett mappajat ahol az eroforras allomanyok vannak..
	 * @return String
	 */
	public String getResourceFolder() {
		return this.resFolder;
	}
	
	/**
	 * Visszaadja a projekt gyokerkonyvtarat.
	 * @return String
	 */
	public String getFolder() {
		return this.folder;
	}
	
	/**
	 * Visszaadja a projekt nevet.
	 * @return String
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Visszaadja a projekt Google Play ertekeleset. 
	 * @return float
	 */
	public float getRating() {
		return this.rating;
	}

	/**
	 * Visszaadja a projekt Google Play telepiteseinek szamat. 
	 * @return int
	 */
	public int getNumberOfInstallations() {
		return this.installations;
	}
}