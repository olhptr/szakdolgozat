package hu.olhptr.szakdolgozat;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * A projektek menedzseleset vegezzuk vele.
 * Egeszen a beolvasastol, a metrikak vegrehajtasaig.
 */
public class ProjectHandler {
	
	/**
	 *  ArrayList, ebben taroljuk a beolvasott projekteket.
	 */
	private List<Project> projectList = new ArrayList<Project>();
	
	/**
	 *  Celmappa, ahova a *.csv fajlok mentese tortenik.
	 */
	private String destinationFolder = null;
	
	/**
	 *  Ebben a valtozoban taroljuk azt, hogy mennyi olyan projekt van, ahol abszolut nem talaltunk
	 *  a keresesi kriteriumoknak megfelelo mappat.
	 */
	private int emptyProjects = 0;

	/**
	 * ProjectHandler konstruktora.<p>
	 * Beolvassuk a *.txt fajlt soronkent, amely tartalmazza a projektek nevet, utvonalat,
	 * Google Play ertekeleset es a telepitesek szamat.
	 * Majd ezeket taroljuk a Project osztaly segitsegevel, amit ArrayListbe teszunk.
	 * @param inputTxt ebbol olvassuk be a projekteket
	 * @param destinationFolder ebbe a mappaba tortenik az eredmenyek mentese
	 * @throws IOException
	 */
	public ProjectHandler(File inputTxt, File destinationFolder) throws IOException {
		
		FileReader fr = new FileReader(inputTxt);
		LineNumberReader lnr = new LineNumberReader(fr);

		String currentLine = null;
		while ((currentLine=lnr.readLine()) != null) {
			String[] separated = currentLine.split(";");
			String name = separated[0];
			String folder = separated[1];
			float rating = Float.valueOf(separated[2].trim()).floatValue();
			int installations = Integer.parseInt(separated[3]);
			this.projectList.add(new Project(name, folder, rating, installations));
		}
		lnr.close();
		
		this.destinationFolder = destinationFolder.getAbsolutePath();
	}
	
	/**
	 * Vegigjarjuk a projekteket, majd a meghatarozott metrikak szamitasat vegrahajtuk rajtuk.
	 * Majd pedig fajlba iratas tortenik, majd pedig az adott xmlrepertory "destruktoranak" hivasa.
	 * @throws IOException
	 */
	public void executeActions() throws IOException {
		System.out.println("yyyy-MM-dd HH:mm:ss  -  Projekt neve [XML fajlok szama]");
		for (Project storedProject : this.projectList){
			String thisFile = storedProject.getName();
			System.out.print(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "  -  " + thisFile);
			XMLRepertory xmlRepertory = new XMLRepertory(storedProject);
			if (!xmlRepertory.isEmpty()){
				// Google Play ertekelesek es letoltesek szamanak osszegyujtese.
				Result.addThisNumberToSetOfRatings(String.valueOf(Math.round(storedProject.getRating()*10)));
				Result.addThisNumberToSetOfInstallations(String.valueOf(storedProject.getNumberOfInstallations()));
				
				System.out.println(" ["+xmlRepertory.getNumberOfFiles()+"]");
				
				Result results = new Result(xmlRepertory, this.destinationFolder);
				
				// Metrikak eloallitasa.
				Map<String, Integer> LLOC = XMLActions.getLogicalLinesOfCode(xmlRepertory);
				Map<String, Integer> NID =  XMLActions.getNumberOfIDs(xmlRepertory);
				Map<String, Integer> NLE =  XMLActions.getNumberOfNestingLevel(xmlRepertory);
				Map<String, Integer> NEL =  XMLActions.getNumberOfElements(xmlRepertory);
				Map<String, Integer> NIP =  XMLActions.getNumberOfInputElements(xmlRepertory);
				Map<String, Integer> AMM =	XMLActions.getAndroidManifestMetrics(xmlRepertory.getAndroidManifestPath());
				
				// Metrikak eredmenyeinek hozzaadasa.
				results.addThisGeneralMetric(LLOC);
				results.addThisGeneralMetric(NID);
				results.addThisGeneralMetric(NLE);
				results.addThisGeneralMetric(NEL);
				results.addThisGeneralMetric(NIP);
				results.addThisSpecialMetric(AMM);
				
				results.writeIntoCSV(thisFile);
				results.writeIntoGraphXML(thisFile);
				results.storeSumAndAndroidManifestMestricsWithRatingAndInstallation();
			} else {
				System.out.println("[0]");
				++this.emptyProjects;
			}
			XMLActions.setSumsToZero();
			xmlRepertory.killThemWithFire();
		}
		Result.writeIntoARFF(this.destinationFolder);
		Result.setNull();
	}
	
	/**
	 * Beolvasott projektek szama.
	 * @return int
	 */
	public int getNumberOfProjects(){
		return this.projectList.size();
	}
	
	/**
	 * A beolvasott projektek kozul azok, amelyek ervenyesek, tehat legalabb 1 db,
	 * a kriteriumoknak megfelelo *.xml fajlt tartalmaznak.
	 * @return int
	 */
	public int getNumberOfValidProjects(){
		return this.projectList.size()-this.emptyProjects;
	}
}