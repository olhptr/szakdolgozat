package hu.olhptr.szakdolgozat;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;


/**
 * Ezzel az osztallyal valositjuk meg a metrikak hozzaadasat, nyers tarolasat illetve adott formatumba valo menteset.
 */
public class Result {

	/**
	 *  Mentesi fomappa, amit args[1] tartalmaz.
	 */
	private String destinationFolder = null;
	
	/**
	 *  Tarolo az osszes altalanos metrikara.
	 */
	private List<Map<String,Integer>> allGeneralMetrics = null;
	
	/**
	 * Tarolo a specialis metrikakra, ami az AndroidManifest.xml -bol szarmazik.
	 */
	private Map<String,Integer> specialMetrics = null;
	
	/**
	 *  Fajlok szama.
	 */
	private int numberOfFiles;
	
	/**
	 * Projekt Google Play ertekelese.
	 */
	private float rating = 0;
	
	/**
	 * Projekt telepiteseinek szama.
	 */
	private int installations = 0;
	
	/**
	 * Ebben taroljuk az ertekeleseket.
	 */
	private static Set<String> setOfRatings = new HashSet<String>();
	
	/**
	 * Ebben taroljuk a kulonbozo telepitesek szamat.
	 */
	private static Set<String> setOfInstallations = new HashSet<String>();
	
	/**
	 * Ebben taroljuk az osszes beolvasott projekt aggregalt (5db) illetve AndroidManifest.xml (4db) metrikait.
	 */
	private static List<String[]> tarolo = new ArrayList<String[]>();
	
	/**
	 * Result konstruktora<p>
	 * A beolvasott fajlok szama alapjan letrehoz egy tarolot.
	 * @param xmlrepertory *.xml fajlok kontenere
	 * @param destinationFolder a *.csv fajlok mentese ide tortenik
	 */
	public Result (XMLRepertory xmlrepertory, String destinationFolder){
		allGeneralMetrics = new ArrayList<Map<String,Integer>>();
		specialMetrics = new HashMap<String,Integer>();
		this.numberOfFiles=xmlrepertory.getNumberOfFiles();
		this.destinationFolder=destinationFolder;
		this.rating = xmlrepertory.getRating();
		this.installations = xmlrepertory.getNumberOfInstallations();
	}
	
	/**
	 * Kiszamolt altalanos metrika hozzaadasa a gyujtemenyhez.
	 * @param metric metrika
	 */
	public void addThisGeneralMetric (Map<String, Integer> metric){
		this.allGeneralMetrics.add(metric);	
	}

	/**
	 * Kiszamolt specialis, tehat AndroidManifest.xml -bol vett metrika hozzaadasa a gyujtemenyhez.
	 * @param metric metrika
	 */
	public void addThisSpecialMetric (Map<String, Integer> metric){
		this.specialMetrics = metric;
	}
	
	/**
	 * Visszadja, hogy hanyfele altalanos metrikat keszitunk a fajlokrol.
	 * @return int
	 */
	public int getNumberOfGeneralMetrics (){
		return this.allGeneralMetrics.size();
	}
	
	/**
	 * Visszadja, hogy hanyfele specialis metrikat keszitunk az AndroidManifest.xml fajlbol.
	 * @return int
	 */
	public int getNumberOfSpecialMetrics (){
		return this.specialMetrics.size();
	}
	
	/**
	 * Az ArrayListbol kiszedegeti az adatokat es atrakja egy String tombbe.
	 * Igy konnyebb a kiiratas.
	 * @return String[]
	 */
	private String[] makeStringsFromAllGeneralMetrics (){
		String[] resultLines = null;
		if (this.numberOfFiles>0){
			resultLines = new String[this.numberOfFiles];
			for (int i = 0; i < resultLines.length; ++i){
				resultLines[i]="";					
			}
		}

		// Ez elegge "do not touch, magic!" tipusu kod.
		// Tlkeppen sorokat gyartunk, aminek az elso oszlopaban levo nevet, csak egyszer kell hozzadni a sorhoz.
		boolean first = true;
		for (Map <String,Integer> metrics : this.allGeneralMetrics){
			int i = 0;
				for (Map.Entry<String, Integer> entry : metrics.entrySet()) {
					resultLines[i] = first ? entry.getKey()+";"+ entry.getValue() : resultLines[i]+";"+entry.getValue();
					++i;
				}
			first = false;		
		}
		
		return resultLines;
	}
	

	/**
	 * Az AndroidManifest.xml segitsegevel meghatarozott metrikakat belerakjuk egy String tombbe.
	 * @return String[]
	 */
	private String[] makeStringsFromSpecMetrics(){
		String[] resultLines = new String[2];
		resultLines[0]="";					
		resultLines[1] = "AndroidManifest.xml";
		
		for (Map.Entry<String, Integer> entry : specialMetrics.entrySet()) {
			resultLines[0] += ";"+entry.getKey();
			resultLines[1] += ";"+entry.getValue();
		}
		
		return resultLines;
	}
	
	/**
	 * Az osszes eredmeny *.csv fajlba iratasa.
	 * @param fileName fajl neve
	 */
	public void writeIntoCSV(String fileName) {
		try {
			// Mentesi beallitasok.
			File folder = new File(destinationFolder + "/" + "CSV");
			if (!folder.exists() || !folder.isDirectory()) folder.mkdir();
			File file = new File(folder + "/" + fileName+".csv");
			if (!file.exists()) file.createNewFile();
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write("LayoutName;LLOC;NID;NLE;NEL;NIP");
			bw.newLine();
			String[] resultlines = makeStringsFromAllGeneralMetrics();

		    for ( int i = 0; i < makeStringsFromAllGeneralMetrics().length; i++){      
		    	bw.write(resultlines[i]);
		    	bw.newLine();
		    }
		    
		    bw.write("SUM" + ";" + XMLActions.getSumOfMetrics());
		    
		    if (this.specialMetrics.size() > 0){
			    bw.newLine();
			    bw.newLine();
			    resultlines = makeStringsFromSpecMetrics();
			    for ( int i = 0; i < makeStringsFromSpecMetrics().length; i++){      
			    	bw.write(resultlines[i]);
			    	bw.newLine();
			    }
		    }
		    
		    bw.newLine();
		    bw.write("Google Play Rating" + ";" + Float.toString(this.rating).replace(".",","));	
		    
		    bw.newLine();
		    bw.write("Installations" + ";" + Integer.toString(this.installations));
		    
			bw.close();
			fw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Az osszes eredmeny *.xml fajlba iratasa a megfelelo grafos reprezentacio formatumaban.
	 * @param fileName fajl neve
	 */	
	public void writeIntoGraphXML(String fileName) {
		try {
			// 2 dimenzios tombbe pakolgatjuk az eddigi altalanos metrikakat
			String[] resultlines = this.makeStringsFromAllGeneralMetrics();
			String[][] arrayOfGeneralMetrics = new String[this.numberOfFiles][this.getNumberOfGeneralMetrics()];
		    for ( int i = 0; i < this.numberOfFiles; i++){  
		    	String[] array = resultlines[i].split(";");
		    	arrayOfGeneralMetrics[i]=array;	
		    }
		    
		    // 1 dimenziosba pedig a specialis metrikakat az AndroidManifest.xml fajlbol
		    String[] arrayOfSpecMetrics = this.makeStringsFromSpecMetrics()[1].split(";");
		    
		    // Masik 1 dimenziosba pedig az aggregalt metrikakat
		    String[] arrayOfSum = XMLActions.getSumOfMetrics().split(";");
		    
		    
			Element graph = new Element("graph");
			Document doc = new Document(graph);
			
			// Header letrehozasa.
			Element header = new Element("header");
			header.addContent(new Element("info").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("name", "ProjectName"), new Attribute("value", fileName)))));
			header.addContent(new Element("info").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("name", "NumberOfFiles"), new Attribute("value", Integer.toString(this.numberOfFiles))))));
			header.addContent(new Element("info").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("name", "NumberOfGeneralMetrics"), new Attribute("value", Integer.toString(this.getNumberOfGeneralMetrics()))))));
			header.addContent(new Element("info").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("name", "NumberOfSpecialMetrics"), new Attribute("value", Integer.toString(this.getNumberOfSpecialMetrics()))))));

			doc.getRootElement().addContent(header);
			
			// Altalanos metrikak.
			Element data = new Element("data");

		    
		    // Aggregalt es AndroidManifest metrikak.
			String[] headerOfMetrics = {"LLOC", "NID", "NLE", "NEL", "NIP"};
	    	Element rootNode = new Element("node").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("name", "rootNode"), new Attribute("type", "statistics"))));
	    	for (int i = 0; i < this.getNumberOfGeneralMetrics(); ++i){
		    	rootNode.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"), new Attribute("name", headerOfMetrics[i]), new Attribute("context", "metric"), new Attribute("value", arrayOfSum[i])))));	    		
	    	}
	    	rootNode.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"),   new Attribute("name", "Activities"),          new Attribute("context", "metric"), new Attribute("value", arrayOfSpecMetrics[1])))));
	    	rootNode.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"),   new Attribute("name", "All Permissions"),     new Attribute("context", "metric"), new Attribute("value", arrayOfSpecMetrics[2])))));
	    	rootNode.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"),   new Attribute("name", "Inside Permissions"),  new Attribute("context", "metric"), new Attribute("value", arrayOfSpecMetrics[3])))));
	    	rootNode.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"),   new Attribute("name", "Outside Permissions"), new Attribute("context", "metric"), new Attribute("value", arrayOfSpecMetrics[4])))));
	    	rootNode.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "float"), new Attribute("name", "Google Play Rating"),  new Attribute("context", "metric"), new Attribute("value", Float.toString(this.rating))))));	    	
	    	rootNode.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"),   new Attribute("name", "Installations"),       new Attribute("context", "metric"), new Attribute("value", Integer.toString(this.installations))))));	    	
	    	data.addContent(rootNode); 
	    	
	    	
	    	for ( int i = 0; i < this.numberOfFiles; i++){
		    	rootNode.addContent(new Element("edge").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "LogicalView"), new Attribute("direction", "directional"), new Attribute("to", arrayOfGeneralMetrics[i][0])))));	    		
	    	}
	    	
		    for ( int i = 0; i < this.numberOfFiles; i++){
		    	Element node = new Element("node").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("name", arrayOfGeneralMetrics[i][0]), new Attribute("type", "layoutfile"))));
		    	node.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"), new Attribute("name", "LLOC"), new Attribute("context", "metric"), new Attribute("value", arrayOfGeneralMetrics[i][1])))));
		    	node.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"), new Attribute("name", "NID"),  new Attribute("context", "metric"), new Attribute("value", arrayOfGeneralMetrics[i][2])))));
		    	node.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"), new Attribute("name", "NLE"),  new Attribute("context", "metric"), new Attribute("value", arrayOfGeneralMetrics[i][3])))));
		    	node.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"), new Attribute("name", "NEL"),  new Attribute("context", "metric"), new Attribute("value", arrayOfGeneralMetrics[i][4])))));
		    	node.addContent(new Element("attribute").setAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute("type", "int"), new Attribute("name", "NIP"),  new Attribute("context", "metric"), new Attribute("value", arrayOfGeneralMetrics[i][5])))));
		    	rootNode.addContent(node); 
		    }	
	    	
		    doc.getRootElement().addContent(data);
		    
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			
			// Mentesi beallitasok.
			File folder = new File(destinationFolder + "/" + "GRAPH_XML");
			if (!folder.exists() || !folder.isDirectory()) folder.mkdir();
			File file = new File(folder + "/" + fileName + ".xml");
			if (!file.exists()) file.createNewFile();
			xmlOutput.output(doc, new FileWriter(file.getAbsoluteFile()));
	
		} catch (IOException e) {
			e.printStackTrace();
	  }	
	}
	
	
	/**
	 * Egy statikus valtozoban taroljuk az osszes beolvasott projekt aggregalt (5db) illetve AndroidManifest (4db) metrikait.
	 * Ehhez hozzacsatoljuk az adott projekt Google Play ertekeleset és a telepitesek szamat numeric es nominal formaban.
	 * Osszesen 13 db mezo.
	 */
	public void storeSumAndAndroidManifestMestricsWithRatingAndInstallation() {
		String[] AMM = Arrays.copyOfRange(this.makeStringsFromSpecMetrics()[1].split(";"), 1, 5);
		Result.tarolo.add((	XMLActions.getSumOfMetrics() + ";" // LLOC, NID, NLE, NEL, NIP
							+ AMM[0] + ";" // Activities
							+ AMM[1] + ";" // Inside Permissions
							+ AMM[2] + ";" // Outside Permissions
							+ AMM[3] + ";" // All Permissions
							+ Float.toString(this.rating) + ";" // Google Play ertekeles (numeric)
							+ Integer.toString(Math.round((this.rating*10))) + ";" // Google Play ertekeles felszorozva 10-zel == igy lesz belole nominal tipus
							+ Integer.toString(this.installations) + ";" // Google Play letoltesek szama (numeric)
							+ Integer.toString(this.installations)).split(";")); // Google Play letoltesek szama (egyelore numeric, de nominal lesz)
	}

	
	/**
	 * Az osszes project aggregalt illetve AndoridManifest metrikajanak *.arff fajlba iratasa.
	 * Illetve a Google Play ertekeles és telepitesek szamanak mentese numeric es nominal formaban.
	 * @param String mentesi hely
	 */		
	public static void writeIntoARFF(String destinationFolder) {
	
		// Mindketto HashSetbol csinalunk ArrayListet, ugyanis nominal tipusnal ilyet tudunk atadni 
		// a weka.core.Attribute tipusnak masodik parameterkent.
		List<String> ratings = new ArrayList<String>(Result.setOfRatings);
		List<String> installations = new ArrayList<String>(Result.setOfInstallations);
		
		// Attributumok.
	    ArrayList<weka.core.Attribute> attributes = new ArrayList<weka.core.Attribute>((Arrays.asList(
	    		new weka.core.Attribute("lloc"),
	    		new weka.core.Attribute("nid"),
	    		new weka.core.Attribute("nle"),
	    		new weka.core.Attribute("nel"),
	    		new weka.core.Attribute("nip"),
	    		new weka.core.Attribute("activites"),
	    		new weka.core.Attribute("all-permissions"),
	    		new weka.core.Attribute("inside-permissions"),
	    		new weka.core.Attribute("outside-permissions"),
	    		new weka.core.Attribute("google-play-rating-numeric"),
	    		new weka.core.Attribute("google-play-rating-nominal", ratings),
	    		new weka.core.Attribute("installations-numeric"),
	    		new weka.core.Attribute("installations-nominal", installations))));
	    
	    Instances data = new Instances("projects", attributes, 0);
	    double[] attributeValues;
	    
		for (String[] s : Result.tarolo){
			attributeValues = new double[s.length];
			for (int i = 0; i < 10; ++i){
				attributeValues[i] = Double.valueOf(s[i].trim()).doubleValue();
			}
			attributeValues[10] = ratings.indexOf(s[10].trim()); // mivel nominal, ezert index alapjan kell visszakeresni a ratings ArrayListbol
			attributeValues[11] = Double.valueOf(s[11].trim()).doubleValue();
			attributeValues[12] = installations.indexOf(s[12].trim()); // mivel nominal, ezert index alapjan kell visszakeresni az installations ArrayListbol
			data.add(new DenseInstance(1.0, attributeValues));
		}
		
		//Mentesi beallitasok.
		File folder = new File(destinationFolder);
		if (!folder.exists() || !folder.isDirectory()) folder.mkdir();
		File file = new File(folder + "/" + "projects.arff");
	    ArffSaver saver = new ArffSaver();
	    saver.setInstances(data);
	    try {
			saver.setFile(file);
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Ennek az eljarasnak a segitsegevel taroljuk a telepitesek szamat egy HashSetben.
	 * @param installation telepitesek szama
	 */
	public static void addThisNumberToSetOfInstallations(String installation ){
		Result.setOfInstallations.add(installation);
	}

	/**
	 * Ennek az eljarasnak a segitsegevel tartoljuk az ertekeleseket egy HashSetben.
	 * @param rating ertekeles
	 */
	public static void addThisNumberToSetOfRatings(String rating){
		Result.setOfRatings.add(rating);
	}
	
	/** 
	 * Adott statikus valtozokat allit nullra hasznalat utan.
	 */
	public static void setNull(){
		Result.tarolo = null;
		Result.setOfRatings = null;
		Result.setOfInstallations = null;
	}
}