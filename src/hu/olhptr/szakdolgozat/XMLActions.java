package hu.olhptr.szakdolgozat;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;


public class XMLActions {

	/**
	 * Android namespace
	 */ 
	private final static Namespace ns = Namespace.getNamespace("android", "http://schemas.android.com/apk/res/android");

	/**
	 *  Android input mezok
	 */
	private final static List <String> inputFields = Arrays.asList(new String[] {
			"AutoCompleteTextView",
			"Button",
			"CheckBox",
			"DatePicker",
			"EditText",
			"ImageButton",
			"MultiAutoCompleteTextView",
			"RadioGroup",
			"RatingBar",
			"Spinner",
			"Switch",
			"TimePicker",
			"ToggleButton" });
	
	/**
	 *  Android GUI XML szabalyos kommentblokk nyito/zaro eleme
	 */
	private final static String STARTCOMMENT = "<!--";
	private final static String ENDCOMMENT = "-->";

	/**
	 *  Az elemek keresesere definialt enumeracio.
	 */
	private enum WhatToSearch {
		eachElement, inputElement, activityElement;
	}

	// Segedvaltozok a meroszamoknak.
	private static int LOGICALLINESOFCODE = 0;
	private static int NUMBEROFIDS = 0;
	private static int MAXDEPTHOFXML = 0;
	private static int NUMBEROFELEMENTS = 0;
	private static int NUMBEROFINPUTELEMENTS = 0;
	private static int SUMOFLOGICALLINESOFCODE = 0;
	private static int SUMOFNUMBEROFIDS = 0;
	private static int SUMOFMAXDEPTHOFXML = 0;
	private static int SUMOFNUMBEROFELEMENTS = 0;
	private static int SUMOFNUMBEROFINPUTELEMENTS = 0;
	private static int AM_INSIDEPERMISSIONS = 0;
	private static int AM_PERMISSIONS = 0;
	private static int AM_ACTIVITIES = 0;
	

	/**
	 * A parameterkent kapott valtozoban levo fajlok logikai soranak a szamat hatarozza meg.
	 * @param xmlrepertory XML fajlokat tartalmaz
	 * @return ArrayList
	 * @throws IOException
	 */
	public static Map<String, Integer> getLogicalLinesOfCode(XMLRepertory xmlrepertory) throws IOException {
		
		Map<String, Integer> LLOC = new TreeMap<String, Integer>();
		Iterator<String> iter = xmlrepertory.getFiles().keySet().iterator();

		while(iter.hasNext()) {
			
			String filename = iter.next();
			FileReader fr = new FileReader(xmlrepertory.getFiles().get(filename).getAbsolutePath());
			LineNumberReader lnr = new LineNumberReader(fr);

			String actline = null;
			while ((actline=lnr.readLine()) != null) {
				if (actline.contains(STARTCOMMENT) && actline.contains(ENDCOMMENT) ){						
					lnr.setLineNumber(lnr.getLineNumber() + 1);
				} else if (actline.contains(STARTCOMMENT) && !actline.contains(ENDCOMMENT)){
					while (!actline.contains(ENDCOMMENT)){
						actline = lnr.readLine();
					}
				} else if (actline.trim().isEmpty()){
					lnr.setLineNumber(lnr.getLineNumber() + 1);
				} else {
					++XMLActions.LOGICALLINESOFCODE;
				}

			}
			lnr.close();
			fr.close();
			
			LLOC.put(filename, XMLActions.LOGICALLINESOFCODE);
			XMLActions.SUMOFLOGICALLINESOFCODE += XMLActions.LOGICALLINESOFCODE;
			XMLActions.LOGICALLINESOFCODE = 0;
		}
		return LLOC;
	}



	/**
	 * Rekurziv fuggveny, egy XML fajlban az ID azonositok szamanak
	 * meghatarozasara, statikus valtozot hasznal (XMLActions.NUMBEROFIDS)
	 * @param root kiindulasi gyokerelem
	 * @param numberofids eddig megtalalt "id" attributumu elemek
	 */
	private static void searchIDs(Element root) {

		if (root.hasAttributes() && (root.getAttribute("id", ns) != null)) {
			++XMLActions.NUMBEROFIDS;
		}

		if (root.getChildren().size() > 0) {
			List<Element> rootchildren = root.getChildren();
			Iterator<Element> i = rootchildren.iterator();
			while (i.hasNext()) {
				Element current = i.next();
				searchIDs(current);
			}
		}
	}

	/**
	 * A parameterkent kapott valtozoban levo fajlokban levo ID-k szamat hatarozza meg.
	 * @param xmlrepertory XML fajlokat tartalmaz
	 * @return Map
	 * @throws IOException
	 */
	public static Map<String, Integer> getNumberOfIDs(XMLRepertory xmlrepertory) throws IOException {
		
		Map<String, Integer> NID = new TreeMap<String, Integer>();
		Iterator<String> iter = xmlrepertory.getFiles().keySet().iterator();

		while(iter.hasNext()) {

			String filename = iter.next();

			SAXBuilder sxb = new SAXBuilder();
			Document doc = null;
			try {
				doc = sxb.build(new File(xmlrepertory.getFiles().get(filename).getAbsolutePath()));
			} catch (JDOMException e) {
				e.printStackTrace();
			}

			Element root = doc.getRootElement();
			searchIDs(root);

			NID.put(filename, XMLActions.NUMBEROFIDS);
			XMLActions.SUMOFNUMBEROFIDS += XMLActions.NUMBEROFIDS;
			XMLActions.NUMBEROFIDS = 0;
		}
		return NID;
	}

	/**
	 * Rekurziv fuggveny, meghatarozza a maximum elem melyseget rekurzioval.
	 * @param root kiindulasi gyokerelem
	 * @param level melyseg szama
	 */
	private static void depthOfElement(Element root, int level) {

		if (level > XMLActions.MAXDEPTHOFXML) {
			XMLActions.MAXDEPTHOFXML = level;
		}

		if (root.getChildren().size() > 0) {
			++level;
			List<Element> rootchildren = root.getChildren();
			Iterator<Element> i = rootchildren.iterator();
			while (i.hasNext()) {
				Element current = i.next();
				depthOfElement(current, level);
			}
		}
	}

	
	/**
	 * Visszater mindegyik XML fajl legnagyobb elemmelysegevel. 
	 * @param xmlfiles XML fajlokat tartalmaz
	 * @return Map
	 * @throws IOException
	 */
	public static Map<String, Integer> getNumberOfNestingLevel(XMLRepertory xmlfiles) throws IOException {

		Map<String, Integer> NLE = new TreeMap<String, Integer>();
		Iterator<String> iter = xmlfiles.getFiles().keySet().iterator();

		while(iter.hasNext()) {

			String filename = iter.next();
			int level = 0;
			SAXBuilder sxb = new SAXBuilder();
			Document doc = null;
			try {
				doc = sxb.build(new File(xmlfiles.getFiles().get(filename).getAbsolutePath()));
			} catch (JDOMException e) {
				e.printStackTrace();
			}

			Element root = doc.getRootElement();
			depthOfElement(root, ++level);

			NLE.put(filename, XMLActions.MAXDEPTHOFXML);
			XMLActions.SUMOFMAXDEPTHOFXML += XMLActions.MAXDEPTHOFXML;
			XMLActions.MAXDEPTHOFXML = 0;
		}
		return NLE;
	}

	/**
	 * Rekurziv fuggveny amely tobbfunkcios, tehat tobbfele elemet lehet vele keresni, ezek a WhatToSearch enumeracioban vannak felsorolva.
	 * @param root kiindulasi gyokerelem
	 * @param whatToSearch amilyen elemet akarunk keresni, tehat eachElement, inputElement vagy activityElement
	 */
	private static void countElement(Element root, WhatToSearch whatToSearch) {
		
		if (root.getChildren().size() > 0) {
			List<Element> rootchildren = root.getChildren();
			Iterator<Element> i = rootchildren.iterator();
			switch(whatToSearch) {
				case eachElement:
					XMLActions.NUMBEROFELEMENTS += root.getChildren().size();
					while (i.hasNext()) {
						Element current = i.next();
						countElement(current, whatToSearch);
					}
					break;
				case inputElement:
					while (i.hasNext()) {
						Element current = i.next();
						if (XMLActions.inputFields.contains(current.getName())) {
							++XMLActions.NUMBEROFINPUTELEMENTS;
						}
						countElement(current, whatToSearch);
					}
					break;
				case activityElement:
					while (i.hasNext()) {
						Element current = i.next();
						if ("application".equals(current.getName())) {
							XMLActions.AM_ACTIVITIES =  current.getChildren("activity").size();
						}
					}
					break;
			}
		}
	}

	
	/**
	 * Visszater mindegyik XML fajl legnagyobb elemszamaval.
	 * @param xmlfiles XML fajlokat tartalmaz
	 * @return Map
	 * @throws IOException
	 */
	public static Map<String, Integer> getNumberOfElements(XMLRepertory xmlfiles) throws IOException {

		Map<String, Integer> NEL = new TreeMap<String, Integer>();
		Iterator<String> iter = xmlfiles.getFiles().keySet().iterator();
		WhatToSearch whatToSearch = null;
		
		while(iter.hasNext()) {
			String filename = iter.next();
			SAXBuilder sxb = new SAXBuilder();
			Document doc = null;
			try {
				doc = sxb.build(new File(xmlfiles.getFiles().get(filename).getAbsolutePath()));
			} catch (JDOMException e) {
				e.printStackTrace();
			}
			++XMLActions.NUMBEROFELEMENTS;
			Element root = doc.getRootElement();
			whatToSearch = WhatToSearch.valueOf("eachElement"); countElement(root, whatToSearch);

			NEL.put(filename, XMLActions.NUMBEROFELEMENTS);
			XMLActions.SUMOFNUMBEROFELEMENTS += XMLActions.NUMBEROFELEMENTS;
			XMLActions.NUMBEROFELEMENTS = 0;
		}
		return NEL;
	}


	/**
	 * Visszater mindegyik XML fajl input elemeinek szamaval.
	 * @param xmlfiles XML fajlokat tartalmaz
	 * @return Map
	 * @throws IOException
	 */
	public static Map<String, Integer> getNumberOfInputElements(XMLRepertory xmlfiles) throws IOException {

		Map<String, Integer> NIP = new TreeMap<String, Integer>();
		Iterator<String> iter = xmlfiles.getFiles().keySet().iterator();
		WhatToSearch whatToSearch = null;

		while(iter.hasNext()) {
			String filename = iter.next();
			SAXBuilder sxb = new SAXBuilder();
			Document doc = null;
			try {
				doc = sxb.build(new File(xmlfiles.getFiles().get(filename).getAbsolutePath()));
			} catch (JDOMException e) {
				e.printStackTrace();
			}
			Element root = doc.getRootElement();
			whatToSearch = WhatToSearch.valueOf("inputElement"); countElement(root, whatToSearch);

			NIP.put(filename, XMLActions.NUMBEROFINPUTELEMENTS);
			XMLActions.SUMOFNUMBEROFINPUTELEMENTS += XMLActions.NUMBEROFINPUTELEMENTS;
			XMLActions.NUMBEROFINPUTELEMENTS = 0;
		}
		return NIP;
	}


	/**
	 * Rekurziv fuggveny, getAndroidManifestMetrics() fgv. alfuggvenye,
	 * segitsegevel a jogosultsagokat keressuk ki az AndroidManifest.xml fajlbol.
	 * @param root kiindulasi gyokerelem
	 */
	private static void countPermissions(Element root) {		
		if (root.getChildren().size() > 0) {
			List<Element> rootchildren = root.getChildren();
			Iterator<Element> i = rootchildren.iterator();
			while (i.hasNext()) {
				Element current = i.next();
				if ("uses-permission".equals(current.getName())) {
					++XMLActions.AM_PERMISSIONS;
					String attribute = null;
					if ((attribute = current.getAttributeValue("name", ns)) != null && (attribute.contains("android.permission."))){
						++XMLActions.AM_INSIDEPERMISSIONS;
					}
				}
				countPermissions(current);
			}
		}
	}
	
	
	/**
	 * AndroidManifest.xml fajlbol allit elo Activities, In/Outside illetve All permissions metrikakat.
	 * @param androidManifestPath AndroidManifest.xml fajl eleresi utvonala
	 * @return Map
	 * @throws IOException
	 */
	public static Map<String, Integer> getAndroidManifestMetrics(String androidManifestPath) throws IOException {

		Map<String, Integer> AMM = new TreeMap<String, Integer>();
		WhatToSearch whatToSearch = null;

		File androidManifest = new File(androidManifestPath +"/AndroidManifest.xml");
		if (androidManifest.isFile()){
			
			SAXBuilder sxb = new SAXBuilder();
			Document doc = null;
			try {
				doc = sxb.build(new File(androidManifest.getAbsolutePath()));
			} catch (JDOMException e) {
				e.printStackTrace();
			}
			Element root = doc.getRootElement();
			countPermissions(root);
			whatToSearch = WhatToSearch.valueOf("activityElement"); countElement(root, whatToSearch);
			
			AMM.put("Activities", XMLActions.AM_ACTIVITIES);
			AMM.put("Inside Permissions", XMLActions.AM_INSIDEPERMISSIONS);
			AMM.put("Outside Permissions", XMLActions.AM_PERMISSIONS-XMLActions.AM_INSIDEPERMISSIONS);
			AMM.put("All Permissions", XMLActions.AM_PERMISSIONS);
			
			XMLActions.AM_PERMISSIONS = 0;
			XMLActions.AM_INSIDEPERMISSIONS = 0;
			XMLActions.AM_ACTIVITIES = 0;
			
		}
		return 	AMM;
	}	
	
	/**
	 * Adott statikus valtozokat allit 0 -ra hasznalat utan.
	 */
	public static void setSumsToZero(){
		XMLActions.SUMOFLOGICALLINESOFCODE = 0;
		XMLActions.SUMOFNUMBEROFIDS = 0;
		XMLActions.SUMOFMAXDEPTHOFXML = 0;
		XMLActions.SUMOFNUMBEROFELEMENTS = 0;
		XMLActions.SUMOFNUMBEROFINPUTELEMENTS = 0;		
	}
	
	/**
	 * Stringge alakitja az altalanos metrikak osszeget.
	 * @return String
	 */
	public static String getSumOfMetrics(){
		return 	XMLActions.SUMOFLOGICALLINESOFCODE + ";" + 
				XMLActions.SUMOFNUMBEROFIDS + ";" + 
				XMLActions.SUMOFMAXDEPTHOFXML + ";" + 
				XMLActions.SUMOFNUMBEROFELEMENTS + ";" + 
				XMLActions.SUMOFNUMBEROFINPUTELEMENTS;
	}
}