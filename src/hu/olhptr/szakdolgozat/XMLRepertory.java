package hu.olhptr.szakdolgozat;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * Ezzel az osztallyal egy adott projekthez tartozo, a felteteleknek megfelelo *.xml fajlokat keressuk meg, majd taroljuk.
 */
public class XMLRepertory {

	private static Map<String, File> xmlFiles = null;
	private static String xmlDirPath = null;
	private String androidManifestPath = null;
	private float projectRating;
	private int installations;
	
	/**
	 * Meghatarozzuk azokat a mappakat, amikben biztos, hogy nincs semmifele adat felulettel kapcsolatban
	 * ilyen pl. az ertekek tarolasara valo values* mappa.
	 */
	private final static List <String> ignoredDirNames = Arrays.asList(new String[] {"values", "drawable", "anim", "raw"});
	
	/**
	 * XMLRepertory konstruktora.
	 * @param project egy Android projekt
	 */
	public XMLRepertory (Project project){
		xmlFiles = new TreeMap<String, File>();
		xmlDirPath = project.getResourceFolder();
		this.androidManifestPath = project.getFolder();
		this.projectRating = project.getRating();
		this.installations = project.getNumberOfInstallations();
		searchXMLFiles(new File(xmlDirPath));
	}
	
	/**
	 * Rekurziv modszer az osszes *.xml fajl kiszuresere, ami nem a kihagyott mappakban vannak.
	 * @param xmldir ebben a mappaban fogunk keresni
	 */
	public static void searchXMLFiles(File xmldir){
        
		if (xmldir.isDirectory()) {
			
			boolean contains = false;

			// Megnezzuk, hogy az adott mappa tartalmaz-e olyan mappanevet, amit nem akarunk szkennelni.
			for (String temp : ignoredDirNames) {
				if (xmldir.getName().contains(temp)) contains = true;
			}
			
			// Ha nem tartalmaz, akkor list valtozoban letaroljuk az osszes *.xml fajlt,
			// ami ebben a mappaban volt.
			if (!contains){
				File list[] = xmldir.listFiles(new FilenameFilter() {
				    public boolean accept(File xmldir, String name) {
				        return name.toLowerCase().endsWith(".xml");
				    }
				});

				// Fajl nevehez pedig az fomappabol elerheto mappak nevet is hozzadjuk.
				// Igy tudjuk biztositani azt, hogy ha esetleg ugyanolyan nevu fajlok vannak kulonbozo mappakban,
				// ne legyen hiba, ugyanis TreeMap adatszerkezetben taroljuk ezeket a fajlokat.	
				for(File f : list){
					if(f.isFile()){
						String fileName = (f.getAbsolutePath().substring(xmlDirPath.length()+1) );
						fileName = fileName.replaceAll("\\\\", "/");
						xmlFiles.put(fileName, f);
					}
				}
				
				String[] sub = xmldir.list();
				for (int i=0; i<sub.length; i++) {
					searchXMLFiles(new File(xmldir, sub[i]));
				}
			}
		}	
	}
	
	
	/**
	 * Visszater a beolvasott *.xml fajlok darabszamaval.
	 * @return int
	 */
	public int getNumberOfFiles(){
		return xmlFiles.size();
	}
	
	/**
	 * AndroidManifest.xml fajl abszolut eleresi utvonala.
	 * @return String
	 */
	public String getAndroidManifestPath() {
		return androidManifestPath;
	}
	
	/**
	 * Projekt Google Play ertekelese [0-5].
	 * @return float
	 */
	public float getRating() {
		return this.projectRating;
	}
	
	/**
	 * Projekt Google Play telepiteseinek szama.
	 * @return int
	 */
	public int getNumberOfInstallations() {
		return this.installations;
	}

	/**
	 * Sikerult-e talalni XML fajlokat.
	 * @return boolean
	 */
	public boolean isEmpty(){
		return xmlFiles.isEmpty();
	}
	
	/**
	 * A beolvasott XML fajlok lekerese.
	 * @return Map<String,File>
	 */
	public Map<String, File> getFiles (){
		return xmlFiles;
	}
	
	/**
	 * A beolvasott XML fajlok lekerese.
	 * @return Map<String,File>
	 */
	public String getXmlDirPath (){
		return xmlDirPath;
	}
	
	/**
	 * Mivel statikus valtozok, ezért muszaj oket null -ra beallitani, kulonben helytelen eredmenyt kapnank.
	 */
	public void killThemWithFire(){
		xmlFiles = null;
		xmlDirPath = null;		
	}
}