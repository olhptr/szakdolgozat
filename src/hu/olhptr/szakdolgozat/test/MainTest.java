package hu.olhptr.szakdolgozat.test;

import static org.junit.Assert.*;

import hu.olhptr.szakdolgozat.Main;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;

public class MainTest {
	@Rule
	public final StandardOutputStreamLog log = new StandardOutputStreamLog();
	
	@Test
	public void testMainArgs1() {
		String args[] = new String[1];
		Main.main(args);
		assertEquals("Keves argumentumot adtal meg!"+System.lineSeparator(), log.getLog());
	}
	
	@Test
	public void testMainArgs3() {
		String args[] = new String[3];
		Main.main(args);
		assertEquals("Sok argumentum, pontosan 2 -t adj meg!"+System.lineSeparator(), log.getLog());
	}
}