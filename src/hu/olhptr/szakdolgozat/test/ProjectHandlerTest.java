package hu.olhptr.szakdolgozat.test;

import static org.junit.Assert.*;

import hu.olhptr.szakdolgozat.ProjectHandler;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProjectHandlerTest {
	
	ProjectHandler projectHandler = null;
	
	@Before
	public void setUp() throws Exception {
		File inputTxt = new File("d:\\#WORKSPACE\\Szakdolgozat\\src\\hu\\olhptr\\szakdolgozat\\test\\test_input.txt");
		File destinationFolder = new File("d:\\#WORKSPACE\\Szakdolgozat\\src\\hu\\olhptr\\szakdolgozat\\test\\test_results");
		projectHandler = new ProjectHandler(inputTxt, destinationFolder);
	}

	@After
	public void tearDown() throws Exception {
		projectHandler = null;
	}

	@Test
	public void testGetNumberOfProjects() {
		assertEquals(3, projectHandler.getNumberOfProjects());
	}

	@Test
	public void testGetNumberOfValidProjects() {
		assertEquals(3, projectHandler.getNumberOfValidProjects());
	}
}