package hu.olhptr.szakdolgozat.test;

import static org.junit.Assert.*;
import hu.olhptr.szakdolgozat.Project;

import org.junit.Before;
import org.junit.Test;

public class ProjectTest {

	private String name = null;
	private String folder = null;
	private String resFolder = null;
	private float rating;
	private int installations;
	private Project myProject = null;
	
	@Before
	public void setUp(){
		this.name = "alarmclock";
		this.folder = "d:/#WORKSPACE/Szakdolgozat/forrasok/Systems/alarmclock";
		this.resFolder = folder + "/res";
		this.rating = 4.5f;
		this.installations = 30000;
		myProject = new Project(this.name, this.folder, this.rating, this.installations);
	}

	@Test
	public void testProject() {
		assertNotNull(myProject);
	}

	@Test
	public void testGetResourceFolder() {
		assertEquals(resFolder, myProject.getResourceFolder());
	}

	@Test
	public void testGetFolder() {
		assertEquals(folder, myProject.getFolder());
	}

	@Test
	public void testGetName() {
		assertEquals(name, myProject.getName());
	}

	@Test
	public void testGetRating() {
		assertEquals(rating, myProject.getRating(), 0.0f);
	}

	@Test
	public void testGetNumberOfInstallations() {
		assertEquals(installations, myProject.getNumberOfInstallations());
	}
}