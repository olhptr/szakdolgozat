package hu.olhptr.szakdolgozat.test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Map;

import hu.olhptr.szakdolgozat.Project;
import hu.olhptr.szakdolgozat.Result;
import hu.olhptr.szakdolgozat.XMLActions;
import hu.olhptr.szakdolgozat.XMLRepertory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ResultTest {
	
	private Project myProject = null;
	private XMLRepertory myXMLRepertory = null;
	private Result results = null;
	
	@Before
	public void setUp() throws Exception {
		myProject = new Project("alarmclock", "d:/#WORKSPACE/Szakdolgozat/forrasok/Systems/alarmclock", 4.5f, 30000);
		myXMLRepertory = new XMLRepertory(myProject);
		String xmlDirPath = myProject.getResourceFolder();
		XMLRepertory.searchXMLFiles(new File(xmlDirPath));
		results = new Result(myXMLRepertory, "d:\\#WORKSPACE\\Szakdolgozat\\src\\hu\\olhptr\\szakdolgozat\\test\\test_results");
		
		Map<String, Integer> LLOC = XMLActions.getLogicalLinesOfCode(myXMLRepertory);
		Map<String, Integer> NID =  XMLActions.getNumberOfIDs(myXMLRepertory);
		Map<String, Integer> NLE =  XMLActions.getNumberOfNestingLevel(myXMLRepertory);
		Map<String, Integer> NEL =  XMLActions.getNumberOfElements(myXMLRepertory);
		Map<String, Integer> NIP =  XMLActions.getNumberOfInputElements(myXMLRepertory);
		Map<String, Integer> AMM =	XMLActions.getAndroidManifestMetrics(myXMLRepertory.getAndroidManifestPath());
		
		results.addThisGeneralMetric(LLOC);
		results.addThisGeneralMetric(NID);
		results.addThisGeneralMetric(NLE);
		results.addThisGeneralMetric(NEL);
		results.addThisGeneralMetric(NIP);
		results.addThisSpecialMetric(AMM);
	}

	@After
	public void tearDown() {
		Result.setNull();
	}
	
	@Test
	public void testGetNumberOfGeneralMetrics() {
		assertEquals(5, results.getNumberOfGeneralMetrics());
	}

	@Test
	public void testGetNumberOfSpecialMetrics() {
		assertEquals(4, results.getNumberOfSpecialMetrics());
	}
}