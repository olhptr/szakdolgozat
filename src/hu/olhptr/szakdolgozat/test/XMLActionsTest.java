package hu.olhptr.szakdolgozat.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import hu.olhptr.szakdolgozat.XMLActions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class XMLActionsTest {

	private XMLRepertoryTest myXMLRepertoryTest = null;
	
	@Before
	public void setUp() throws Exception {
		myXMLRepertoryTest = new XMLRepertoryTest();
		myXMLRepertoryTest.getXMLRepertory();
	}
	
	@After
	public void tearDown() throws Exception {
		XMLActions.setSumsToZero();
	}

	@Test
	public void testGetLogicalLinesOfCode() {
		try
		{
			Map<String, Integer> LLOC = new TreeMap<String, Integer>();
			LLOC.put("layout/alarm_list.xml", 31);
			LLOC.put("layout/alarm_list_item.xml", 57);
			LLOC.put("layout/custom_lock_screen_dialog.xml", 20);
			LLOC.put("layout/dialog_item.xml", 12);
			LLOC.put("layout/fade_settings_dialog.xml", 51);
			LLOC.put("layout/media_picker_dialog.xml", 46);
			LLOC.put("layout/media_picker_row.xml", 15);
			LLOC.put("layout/name_settings_dialog.xml", 14);
			LLOC.put("layout/notification.xml", 53);
			LLOC.put("layout/numeric_picker.xml", 32);
			LLOC.put("layout/pending_alarms.xml", 9);
			LLOC.put("layout/pending_alarms_item.xml", 5);
			LLOC.put("layout/settings.xml", 37);
			LLOC.put("layout/settings_item.xml", 15);
			LLOC.put("layout/time_picker_dialog.xml", 48);
			LLOC.put("xml/app_settings.xml", 33);
			assertEquals(LLOC, XMLActions.getLogicalLinesOfCode(myXMLRepertoryTest.getXMLRepertory()));
		}catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}

	@Test
	public void testGetNumberOfIDs() {
		try
		{
			Map<String, Integer> NID = new TreeMap<String, Integer>();
			NID.put("layout/alarm_list.xml", 5);
			NID.put("layout/alarm_list_item.xml", 5);
			NID.put("layout/custom_lock_screen_dialog.xml", 2);
			NID.put("layout/dialog_item.xml", 0);
			NID.put("layout/fade_settings_dialog.xml", 3);
			NID.put("layout/media_picker_dialog.xml", 8);
			NID.put("layout/media_picker_row.xml", 2);
			NID.put("layout/name_settings_dialog.xml", 1);
			NID.put("layout/notification.xml", 8);
			NID.put("layout/numeric_picker.xml", 4);
			NID.put("layout/pending_alarms.xml", 1);
			NID.put("layout/pending_alarms_item.xml", 0);
			NID.put("layout/settings.xml", 4);
			NID.put("layout/settings_item.xml", 2);
			NID.put("layout/time_picker_dialog.xml", 6);
			NID.put("xml/app_settings.xml", 0);
			assertEquals(NID, XMLActions.getNumberOfIDs((myXMLRepertoryTest.getXMLRepertory())));
		}catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}

	@Test
	public void testGetNumberOfNestingLevel() {
		try
		{
			Map<String, Integer> NLE = new TreeMap<String, Integer>();
			NLE.put("layout/alarm_list.xml", 2);
			NLE.put("layout/alarm_list_item.xml", 4);
			NLE.put("layout/custom_lock_screen_dialog.xml", 2);
			NLE.put("layout/dialog_item.xml", 1);
			NLE.put("layout/fade_settings_dialog.xml", 3);
			NLE.put("layout/media_picker_dialog.xml", 4);
			NLE.put("layout/media_picker_row.xml", 2);
			NLE.put("layout/name_settings_dialog.xml", 2);
			NLE.put("layout/notification.xml", 3);
			NLE.put("layout/numeric_picker.xml", 2);
			NLE.put("layout/pending_alarms.xml", 2);
			NLE.put("layout/pending_alarms_item.xml", 1);
			NLE.put("layout/settings.xml", 3);
			NLE.put("layout/settings_item.xml", 2);
			NLE.put("layout/time_picker_dialog.xml", 4);
			NLE.put("xml/app_settings.xml", 2);
			assertEquals(NLE, XMLActions.getNumberOfNestingLevel((myXMLRepertoryTest.getXMLRepertory())));
		}catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}

	@Test
	public void testGetNumberOfElements() {
		try
		{
			Map<String, Integer> NEL = new TreeMap<String, Integer>();
			NEL.put("layout/alarm_list.xml", 6);
			NEL.put("layout/alarm_list_item.xml", 9);
			NEL.put("layout/custom_lock_screen_dialog.xml", 4);
			NEL.put("layout/dialog_item.xml", 1);
			NEL.put("layout/fade_settings_dialog.xml", 10);
			NEL.put("layout/media_picker_dialog.xml", 11);
			NEL.put("layout/media_picker_row.xml", 3);
			NEL.put("layout/name_settings_dialog.xml", 3);
			NEL.put("layout/notification.xml", 10);
			NEL.put("layout/numeric_picker.xml", 5);
			NEL.put("layout/pending_alarms.xml", 2);
			NEL.put("layout/pending_alarms_item.xml", 1);
			NEL.put("layout/settings.xml", 7);
			NEL.put("layout/settings_item.xml", 3);
			NEL.put("layout/time_picker_dialog.xml", 9);
			NEL.put("xml/app_settings.xml", 5);
			assertEquals(NEL, XMLActions.getNumberOfElements((myXMLRepertoryTest.getXMLRepertory())));
		}catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}

	@Test
	public void testGetNumberOfInputElements() {
		try
		{
			Map<String, Integer> NIP = new TreeMap<String, Integer>();
			NIP.put("layout/alarm_list.xml", 3);
			NIP.put("layout/alarm_list_item.xml", 1);
			NIP.put("layout/custom_lock_screen_dialog.xml", 2);
			NIP.put("layout/dialog_item.xml", 0);
			NIP.put("layout/fade_settings_dialog.xml", 3);
			NIP.put("layout/media_picker_dialog.xml", 0);
			NIP.put("layout/media_picker_row.xml", 0);
			NIP.put("layout/name_settings_dialog.xml", 1);
			NIP.put("layout/notification.xml", 3);
			NIP.put("layout/numeric_picker.xml", 4);
			NIP.put("layout/pending_alarms.xml", 0);
			NIP.put("layout/pending_alarms_item.xml", 0);
			NIP.put("layout/settings.xml", 3);
			NIP.put("layout/settings_item.xml", 0);
			NIP.put("layout/time_picker_dialog.xml", 2);
			NIP.put("xml/app_settings.xml", 0);
			assertEquals(NIP, XMLActions.getNumberOfInputElements((myXMLRepertoryTest.getXMLRepertory())));
		}catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}

	@Test
	public void testGetAndroidManifestMetrics() {
		try
		{
			Map<String, Integer> AMM = new TreeMap<String, Integer>();
			AMM.put("Activities", 5);
			AMM.put("All Permissions", 5);
			AMM.put("Inside Permissions", 5);
			AMM.put("Outside Permissions", 0);
			assertEquals(AMM, XMLActions.getAndroidManifestMetrics("d:/#WORKSPACE/Szakdolgozat/forrasok/Systems/alarmclock"));
		}catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}

	@Test
	public void testGetSumOfMetrics() throws IOException {
		Map<String, Integer> LLOC = XMLActions.getLogicalLinesOfCode(myXMLRepertoryTest.getXMLRepertory());
		Map<String, Integer> NID =  XMLActions.getNumberOfIDs(myXMLRepertoryTest.getXMLRepertory());
		Map<String, Integer> NLE =  XMLActions.getNumberOfNestingLevel(myXMLRepertoryTest.getXMLRepertory());
		Map<String, Integer> NEL =  XMLActions.getNumberOfElements(myXMLRepertoryTest.getXMLRepertory());
		Map<String, Integer> NIP =  XMLActions.getNumberOfInputElements(myXMLRepertoryTest.getXMLRepertory());
		Map<String, Integer> AMM =	XMLActions.getAndroidManifestMetrics(myXMLRepertoryTest.getXMLRepertory().getAndroidManifestPath());
		assertEquals("478;51;39;89;22", XMLActions.getSumOfMetrics());
	}
	
	@Test
	public void testSetSumsToZero() {
		assertEquals("0;0;0;0;0", XMLActions.getSumOfMetrics());
	}
}