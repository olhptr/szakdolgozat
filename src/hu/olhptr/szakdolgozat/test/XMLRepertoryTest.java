package hu.olhptr.szakdolgozat.test;

import static org.junit.Assert.*;

import hu.olhptr.szakdolgozat.Project;
import hu.olhptr.szakdolgozat.XMLRepertory;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

public class XMLRepertoryTest {

	public static String xmlDirPath = null;
	private XMLRepertory myXMLRepertory = null;
	private Project myProject = null;

	public XMLRepertory getXMLRepertory()
	{
		if (myXMLRepertory == null)
		{
			this.setUp();
		}
		return myXMLRepertory;
	}
	
	@Before
	public void setUp() {
		myProject = new Project("alarmclock", "d:/#WORKSPACE/Szakdolgozat/forrasok/Systems/alarmclock", 4.5f, 30000);
		myXMLRepertory = new XMLRepertory(myProject);
		xmlDirPath = myProject.getResourceFolder();
		XMLRepertory.searchXMLFiles(new File(xmlDirPath));
	}
	
	@Test
	public void testXMLRepertory() {
		assertNotNull(myXMLRepertory);
	}

	@Test
	public void testGetNumberOfFiles() {
		assertEquals(16, myXMLRepertory.getNumberOfFiles());
	}

	@Test
	public void testGetAndroidManifestPath() {
		assertEquals("d:/#WORKSPACE/Szakdolgozat/forrasok/Systems/alarmclock", myXMLRepertory.getAndroidManifestPath(), myProject.getFolder());
	}

	@Test
	public void testGetRating() {
		assertEquals(4.5f, myXMLRepertory.getRating(), 0.0f);
	}

	@Test
	public void testGetNumberOfInstallations() {
		assertEquals(30000, myXMLRepertory.getNumberOfInstallations());
	}

	@Test
	public void testIsEmpty() {
		assertEquals(false, myXMLRepertory.isEmpty());
	}

	@Test
	public void testKillThemWithFire() {
		myXMLRepertory.killThemWithFire();
		assertEquals(null, myXMLRepertory.getFiles());
		assertEquals(null, myXMLRepertory.getXmlDirPath()); 
	}
}